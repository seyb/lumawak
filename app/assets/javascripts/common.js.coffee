$ ->

  $('.flash').overlay(
    top: 20,
    mask:
      color: '#000'
      loadSpeed: 200
      opacity: 0.5
    closeOnClick: true
    load: true
  )
  $('a[href].overlay_button').overlay(
    top: 20,
    mask:
      color: '#000'
      loadSpeed: 200
      opacity: 0.5
    closeOnClick: true
    onBeforeLoad: ->
    onLoad: ->
      $('.deploy.overlay .ajax').toggle()
      $('.deploy.overlay .form form input[type=submit]').click =>
        $('.deploy.overlay .ajax').toggle()
        $('.deploy.overlay .form').toggle()
  )