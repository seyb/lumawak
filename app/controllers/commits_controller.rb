class CommitsController < ApplicationController
  def index
    @commits = Commit.all
  end

  def new
    @commit = Commit.new
  end

  def create
    params.inspect
    unless params[:commit].blank?
      @commit = Commit.create(params[:commit])
      if @commit.valid?
        @commit.save
        redirect_to :action => :index
      else
        render :new
      end
    end
  end

  def edit
    @commit = Commit.find(params[:id])
  end

  def update
    @commit = Commit.find(params[:id])
    if !@commit.blank? && @commit.update_attributes(params[:commit])
      redirect_to :action => 'index'
    else
      render :edit
    end
  end

  def destroy
    Commit.find(params[:id]).destroy
    redirect_to :action => 'index'
  end
end
