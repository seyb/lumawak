require 'yaml'

class ApplicationController < ActionController::Base
  protect_from_forgery
  http_basic_authenticate_with :name => "seyb", :password => "naicie0A"
  before_filter :load_conf, :make_menu

  def load_conf
    @config = YAML.load_file(Rails.root.join("config","config.yml"))[Rails.env]
  end

  def make_menu
    @menu = ['dashboard', 'projects', 'commits']
  end
end
