class ProjectsController < ApplicationController

  def index
    @projects = Project.all
  end

  def dashboard
    @projects = Project.all
  end

  def new
    @project = Project.new
  end

  def show
    @project = Project.find(params[:id])
    logger.debug @project
  end

  def create
    unless params[:project].blank?
      @project = Project.create(params[:project])
      if @project.valid?
        @project.save
        redirect_to :action => :index
      else
        render :new
      end
    end
  end

  def edit
    @project = Project.find(params[:id])
  end

  def browse
    @project = Project.find(params[:id])
    @hb = (params[:hb].blank?) ? "HEAD" : params[:hb].to_s
    @h = (params[:h].blank?) ? "HEAD" : params[:h].to_s
    @gtree = @project.gtree(@h)
  end

  def deploy
    @project = Project.find(params[:id])
    if !params[:h].blank? && !params[:r].blank? && !@project.blank?
      if @project.deploy(params[:h], params[:r], params[:comment])
        flash[:success] = "Felicitations. Le deploiement a ete un succes !"
        redirect_to :action => :deploy
      else
        flash[:error] = "Desole, le deploiement a echoue"
      end
    end
    @commits = {}
    @branches = @project.repository.branches
    @remotes = @project.repository({:init => true}).remotes
    @project.commits.map{|c| @commits[c.number] = c}
  end

  def update
    @project = Project.find(params[:id])
    if !@project.blank? && @project.update_attributes(params[:project])
      flash[:success] = 'Sauvegarde reussie'
      redirect_to :action => 'show'
    else
       render :edit
    end
  end

  def destroy
    Project.find(params[:id]).destroy
    redirect_to :action => 'index'
  end
end