module ProjectsHelper
  def timeago time_date
    a = (Time.now-time_date).to_i
    case a
      when 0 then return "A l\'instant"
      when 1 then return 'Il y a une seconde'
      when 2..59 then return "Il y a #{a.to_s} secondes"
      when 60..119 then return 'Il y a une minute' #120 = 2 minutes
      when 120..3540 then return "Il y a #{(a/60).to_i.to_s} minutes"
      when 3541..7100 then return 'Il y a une heure' # 3600 = 1 hour
      when 7101..82800 then return "Il y a #{((a+99)/3600).to_i.to_s} heures"
      when 82801..172000 then return 'Il y a un jour' # 86400 = 1 day
      when 172001..518400 then return "Il y a #{((a+800)/(60*60*24)).to_i.to_s} jours"
      when 518400..1036800 then return 'Il y a une semaine'
    end
    return "Il y a #{((a+180000)/(60*60*24*7)).to_i.to_s} semaines"
  end

  def gcrumb project, current, search, path={}, init={}
    path = {'HEAD' => current} if path.blank?
    init = {'HEAD' => current} if init.blank?

    unless current == search
      childs = project.gtree(current)[:subtrees]
      if childs.blank?
        path = init
      else
        childs.each do |childname, hash|
          unless path.has_value?(search.to_s)
            path = self.gcrumb(project, hash.to_s, search.to_s, path.merge({childname => hash.to_s}), init)
          end
        end
      end
    end
    return path
  end
end