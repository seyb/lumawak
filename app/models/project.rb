class Project < ActiveRecord::Base
  has_many :commits, :dependent => :destroy

  attr_accessible :name, :description, :color, :projecttype, :url, :gitpath, :path
  validates :name, :uniqueness => true, :presence => true
  validates :url, :uniqueness => true
  validates :gitpath, :presence => true

  def load_conf
    config = YAML.load_file(Rails.root.join("config","config.yml"))[Rails.env]
    return config
  end

  #returns Git repository object
  def repository options={:init => false}
    unless self.gitpath.blank?
      if (options[:init])
        repository = Git.init(self.path) if File.directory?(self.path)
      else
        path = self.load_conf['git_path'].concat(self.gitpath)
        repository = Git.bare(path) if File.directory?(path)
      end
    end
    return repository
  end

  def logs nb=5, options={:bydate => false, :commits => false}
    gcommits = {}
    unless self.repository.blank?
      repository.log(nb).each do |commit_hash|
        gcommits[commit_hash] = repository.gcommit(commit_hash)
      end
    end
    if options[:bydate]
      gcommits = self.logsByDate(gcommits)
    end
    if options[:commits]
      gcommits = self.logCommits(gcommits, options[:commits])
    end
    return gcommits
  end

  def logsByDate gcommits
    unless gcommits.blank?
      commits = {}
      gcommits.each do |commit|
        date = commit.first.author_date.strftime("%d/%m/%Y")
        if commits[date].blank?
          commits[date] = commit
        else
          commits[date].concat(commit)
        end
      end
      return commits
    end
  end

  def logCommits gcommits, optcommits
    unless gcommits.blank?
      commits = {}
      opt = optcommits.map{|e| e.number}
      hash = 'none'
      gcommits.each do |commit|
        hash = commit.first.to_s if opt.include? commit.first.to_s
        if commits[hash].blank?
          commits[hash] = [commit.first]
        else
          commits[hash].concat([commit.first])
        end
      end
      return commits
    end
  end

  def gtree commit="HEAD^{tree}", options={:subtrees => true, :blobs => true}
    tree = {}
    unless self.repository.blank?
      gtree = self.repository.gtree(commit)
      if options[:blobs]
        tree = {:blobs => gtree.blobs}
      end
      if options[:subtrees]
        tree.merge!({:subtrees => gtree.subtrees})
      end
    end
    return tree
  end

  def deploy hash, remote='origin', comment=''
    repo = self.repository({:init => true})
    if hash.blank? && remote.blank? && repo.blank? && repo.remotes.include?(remote)
      return false
    else
      repo.fetch(remote)
      repo.reset_hard(repo.gcommit(hash))
      self.commits.create(:number => hash, :comment => comment)
      return true
    end
  end
end
