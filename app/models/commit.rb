class Commit < ActiveRecord::Base
  # attr_accessible :title, :body
  belongs_to :project

  attr_accessible :number, :comment, :project_id
end
