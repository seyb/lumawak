class CreateCommits < ActiveRecord::Migration
  def change
    create_table :commits do |t|
      t.string :number
      t.text :comment
      t.references :project

      t.timestamps
    end
  end
end
