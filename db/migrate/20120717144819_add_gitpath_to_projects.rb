class AddGitpathToProjects < ActiveRecord::Migration
  def change
    add_column :projects, :gitpath, :string, :after => :url
  end
end
